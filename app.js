var bodyParser = require("body-parser"),
methodOverride = require("method-override"),
LocalStrategy  = require("passport-local"),
mongoose       = require("mongoose"),
passport       = require("passport"),
express        = require("express"),
PhotoFolder    = require("./models/photoFolder"),
PhotoFolderSecondGroup = require("./models/photoFolderSecondGroup"),
Post           = require("./models/post"),
User           = require("./models/user"),
path           = require("path"),
crypto         = require("crypto"),
multer         = require("multer"),
GridFsStorage  = require("multer-gridfs-storage"),
Grid           = require("gridfs-stream"),
app            = express();

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', true);
mongoose.set('useCreateIndex', true);

mongoose.connect("mongodb://localhost/kindergartenv4", { useNewUrlParser: true });

// dodawanie plikow 
const mongoURI = "mongodb://localhost/kindergartenv4";
const conn = mongoose.createConnection(mongoURI);

// App config
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));

//dodawanie plikow
let gfs;

conn.once('open', function () {
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection('uploads');
});

const storage = new GridFsStorage({
  url: mongoURI,
  file: (req, file) => {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const filename = buf.toString('hex') + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          aliases: req.originalUrl,
          bucketName: 'uploads'
        };
        resolve(fileInfo);
      });
    });
  }
});
const upload = multer({ storage });

//Passport config
app.use(require("express-session")({
    secret: "This is my secret",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//Global variable to all routes
app.use(function(req, res, next) {
   res.locals.currentUser = req.user;
   next();
});


// Routes
// POST ROUTES ////////////////////////
// Index route
app.get("/posts", function(req, res) {
    Post.find({}, function(err, posts) {
       if (err) {
            console.log("Error");
       } else {
            res.render("posts/index", {posts: posts});           
       }
    });
});

//New post route
app.get("/posts/new", isLoggedIn, function(req, res) {
   res.render("posts/new"); 
});

//Create post route
app.post("/posts", isLoggedIn, function(req, res) {
    Post.create(req.body.post, function(err, newPost) {
        if (err) {
            res.render("posts/new");
        } else {
            res.redirect("/posts");
        }
    });
});

//Show post page
app.get("/posts/:id", function(req, res) {
   Post.findById(req.params.id, function(err, foundPost) {
      if (err) {
          res.redirect("/posts");
      } else {
          res.render("posts/show", {post: foundPost});
      }
   });
});

//Edit post route
app.get("/posts/:id/edit", isLoggedIn, function(req, res) {
    Post.findById(req.params.id, function(err, foundPost) {
       if (err) {
           res.redirect("/posts");
       } else {
            res.render("posts/edit", {post: foundPost});            
       }
    });

});

//Update post route
app.put("/posts/:id", isLoggedIn, function(req, res) {
   Post.findByIdAndUpdate(req.params.id, req.body.post, function(err, updatedPost) {
       if (err) {
           res.redirect("/posts");
       } else {
           res.redirect("/posts/" + req.params.id);
       }
   }) ;
});

//Delete post route
app.delete("/posts/:id", function(req, res) {
    Post.findByIdAndRemove(req.params.id, function(err) {
       if (err) {
           res.redirect("/posts");
       } else {
           res.redirect("/posts");
       }
    });
});



// GALLERY ROUTES ////////////////////////
// FIRST GROUP
//Index gallery firstgroup route
 app.get("/gallery/firstGroup", function(req, res) {
    PhotoFolder.find({}, function(err, photoFolders) {
       if (err) {
           console.log(err); 
       } else {
           res.render("gallery/firstGroup/firstGroup", {photoFolders: photoFolders});
       }
    });
 });

//New gallery firstgroup route 
 app.get("/gallery/firstGroup/new", isLoggedIn, function(req, res) {
   res.render("gallery/firstGroup/new"); 
});

//Create gallery firstgroup route
app.post("/gallery/firstGroup", isLoggedIn, function(req, res) {
    PhotoFolder.create(req.body.photoFolder, function(err, newPhotoFolder) {
        if (err) {
            res.render("gallery/firstGroup/new");
        } else {
            res.redirect("/gallery/firstGroup");
        }
    });
});

//Show gallery firstgroup route
app.get("/gallery/firstGroup/:idFolder", function(req, res) {
   PhotoFolder.findById(req.params.idFolder, function(err, foundPhotoFolder) {
      if (err) {
          res.redirect("/gallery/firstGroup");
      } else {
        gfs.files.find().toArray((err, files) => {
            if (!files || files.length === 0) {
                res.render("gallery/firstGroup/show", {photoFolder: foundPhotoFolder, files: false});
            } else {
                files.map(file => {
                    if (file.contentType === 'image/jpeg' || file.contentType === 'image/png' || file.contentType === 'image/jpg' || file.contentType === 'image/JPG') {
                        file.isImage = true;
                    } else {
                        file.isImage = false;
                    }
                });
                res.render("gallery/firstGroup/show", {photoFolder: foundPhotoFolder, files: files});
            }
            
        });
        //   res.render("gallery/firstGroup/show", {photoFolder: foundPhotoFolder});
      }
   });
});

//Edit gallery firstgroup route
app.get("/gallery/firstGroup/:idFolder/edit", isLoggedIn, function(req, res) {
    PhotoFolder.findById(req.params.idFolder, function(err, foundPhotoFolder) {
       if (err) {
           res.redirect("/gallery/firstGroup");
       } else {
            res.render("gallery/firstGroup/edit", {photoFolder: foundPhotoFolder});            
       }
    });
});

//Update gallery firstgroup route
app.put("/gallery/firstGroup/:idFolder", isLoggedIn, function(req, res) {
   PhotoFolder.findByIdAndUpdate(req.params.idFolder, req.body.photoFolder, function(err, updatedPhotoFolder) {
       if (err) {
           res.redirect("/gallery/firstGroup");
       } else {
           res.redirect("/gallery/firstGroup/" + req.params.idFolder);
       }
   });
});

//Delete gallery firstgroup route
app.delete("/gallery/firstGroup/:idFolder", function(req, res) {
    PhotoFolder.findByIdAndRemove(req.params.idFolder, function(err) {
       if (err) {
           res.redirect("/gallery/firstGroup");
       } else {
           res.redirect("/gallery/firstGroup");
       }
    });
});
 
 
 
// IMAGES
//Post for upload images
app.post("/gallery/firstGroup/:idFolder", upload.array('file', 10000), isLoggedIn, function(req, res) {
    res.redirect("/gallery/firstGroup/" + req.params.idFolder);
});



//SECOND GROUP
//Index gallery secondgroup route
 app.get("/gallery/secondGroup", function(req, res) {
    PhotoFolderSecondGroup.find({}, function(err, photoFolderSecondGroups) {
       if (err) {
           console.log(err); 
       } else {
           res.render("gallery/secondGroup/secondGroup", {photoFolders: photoFolderSecondGroups});
       }
    });
 });
 
 //New gallery secondgroup route 
 app.get("/gallery/secondGroup/new", isLoggedIn, function(req, res) {
   res.render("gallery/secondGroup/new"); 
});

//Create gallery secondgroup route
app.post("/gallery/secondGroup", isLoggedIn, function(req, res) {
    PhotoFolderSecondGroup.create(req.body.photoFolder, function(err, newPhotoFolder) {
        if (err) {
            res.render("gallery/secondGroup/new");
        } else {
            res.redirect("/gallery/secondGroup");
        }
    });
});

//Show gallery secondgroup route
app.get("/gallery/secondGroup/:idFolder", function(req, res) {
   PhotoFolderSecondGroup.findById(req.params.idFolder, function(err, foundPhotoFolder) {
      if (err) {
          res.redirect("/gallery/secondGroup");
      } else {
        gfs.files.find().toArray((err, files) => {
            if (!files || files.length === 0) {
                res.render("gallery/secondGroup/show", {photoFolder: foundPhotoFolder, files: false});
            } else {
                files.map(file => {
                    if (file.contentType === 'image/jpeg' || file.contentType === 'image/png' || file.contentType === 'image/jpg' || file.contentType === 'image/JPG') {
                        file.isImage = true;
                    } else {
                        file.isImage = false;
                    }
                });
                res.render("gallery/secondGroup/show", {photoFolder: foundPhotoFolder, files: files});
            }
            
        });
        //   res.render("gallery/firstGroup/show", {photoFolder: foundPhotoFolder});
      }
   });
});

//Edit gallery secondgroup route
app.get("/gallery/secondGroup/:idFolder/edit", isLoggedIn, function(req, res) {
    PhotoFolderSecondGroup.findById(req.params.idFolder, function(err, foundPhotoFolder) {
       if (err) {
           res.redirect("/gallery/secondGroup");
       } else {
            res.render("gallery/secondGroup/edit", {photoFolder: foundPhotoFolder});            
       }
    });
});

//Update gallery secondgroup route
app.put("/gallery/secondGroup/:idFolder", isLoggedIn, function(req, res) {
   PhotoFolderSecondGroup.findByIdAndUpdate(req.params.idFolder, req.body.photoFolder, function(err, updatedPhotoFolder) {
       if (err) {
           res.redirect("/gallery/secondGroup");
       } else {
           res.redirect("/gallery/secondGroup/" + req.params.idFolder);
       }
   });
});

//Delete gallery secondgroup route
app.delete("/gallery/secondGroup/:idFolder", function(req, res) {
    PhotoFolderSecondGroup.findByIdAndRemove(req.params.idFolder, function(err) {
       if (err) {
           res.redirect("/gallery/secondGroup");
       } else {
           res.redirect("/gallery/secondGroup");
       }
    });
});

// IMAGES
//Post for upload images
app.post("/gallery/secondGroup/:idFolder", upload.array('file', 10000), isLoggedIn, function(req, res) {
    res.redirect("/gallery/secondGroup/" + req.params.idFolder);
});

//API to all files
app.get("/files", (req, res) => {
    gfs.files.find().toArray((err, files) => {
        if (!files || files.length === 0) {
           return res.status(404).json({
              err: "No files exist" 
           });
        } 
        return res.json(files);
    });
});

//API to single file
app.get("/files/:filename", (req, res) => {
    gfs.files.findOne({filename: req.params.filename}, (err, file) => {
        if (!file || file.length === 0) {
           return res.status(404).json({
              err: "No file exists" 
           });
        }
        return res.json(file);
    });
});

//Image display
app.get("/image/:filename", (req, res) => {
    gfs.files.findOne({filename: req.params.filename}, (err, file) => {
        if (!file || file.length === 0) {
           return res.status(404).json({
              err: "No file exists" 
           });
        }
        if (file.contentType === 'image/jpeg' || file.contentType === 'image/png' || file.contentType === 'image/jpg' || file.contentType === 'image/JPG') {
            const readstream = gfs.createReadStream(file.filename);
            readstream.pipe(res);
        } else {
            res.status(404).json({
               err: "Not an image" 
            });
        }
    });
});

//Image delete
app.delete('/files/:id', (req, res) => {
    gfs.remove({_id: req.params.id, root: "uploads"}, function (err, gridStore) {
        if (err) {
           return res.status(404).json({
                err: err   
            });
        }
        res.redirect('back');
    
    });
});


//Landing page
app.get("/", function(req, res) {
    res.render("landing");
});

//Home page
app.get("/home", function(req, res) {
    res.render("home");
});

//Cadre page
app.get("/cadre", function(req, res) {
    res.render("cadre");
});

//Activities page
app.get("/activities", function(req, res) {
    res.render("activities");
});

//Menu page
app.get("/menu", function(req, res) {
    res.render("menu");
});

//Gallery page
app.get("/gallery", function(req, res) {
    res.render("gallery/gallery");
});

//Contact page
app.get("/contact", function(req, res) {
    res.render("contact");
});

//Download page
app.get("/download", function(req, res) {
    res.render("download");
});

//Recrutation page
app.get("/recrutation", function(req, res) {
    res.render("recrutation");
});


//===============================================
//Authorization routes

//sign up logic -- do zakomentowania
app.get("/register", function(req, res) {
    res.render("register"); 
});

app.post("/register", function(req, res) {
    var newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, function(err, user) {
        if (err) {
            console.log(err);
            return res.render("register");
        }
        passport.authenticate("local")(req, res, function() {
           res.redirect("/posts"); 
        });
    });
});

// show login form
app.get("/login", function(req, res) {
   res.render("login"); 
});

app.post("/login", passport.authenticate("local", {successRedirect: "/posts", failureRedirect: "/login"}), function(req, res) {
    
});

//logout
app.get("/logout", function(req, res) {
   req.logout();
   res.redirect("/posts");
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect("/login");
}

//=====================OBLUGA ZLYCH LINKOW 
// ZROBIC PRZYJMOWANIE NULLOW Z BAZYDANYCH!!!!
app.get("/:anything", function(req, res) {
   res.redirect("/posts"); 
});

//===============================================
//Listener
app.listen(process.env.PORT, process.env.IP);