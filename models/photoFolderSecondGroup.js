var mongoose = require("mongoose");

var PhotoFolderSecondGroupSchema = new mongoose.Schema({
   title: String,
   year: String
});

module.exports = mongoose.model("PhotoFolderSecondGroup", PhotoFolderSecondGroupSchema);