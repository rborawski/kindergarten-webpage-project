var mongoose = require("mongoose");
var moment = require("moment");

moment.locale('pl');

var postSchema = new mongoose.Schema({ 
   title: String,
   body: String,
   created: {type: String, default: moment().format('LL')}
});

module.exports = mongoose.model("Post", postSchema);