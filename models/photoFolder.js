var mongoose = require("mongoose");

var PhotoFolderSchema = new mongoose.Schema({
   title: String,
   year: String
});

module.exports = mongoose.model("PhotoFolder", PhotoFolderSchema);